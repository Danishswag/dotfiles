# Dotfiles

This repo relies on [chezmoi][chezmoi] to manage dotfiles. Start by installing chezmoi, probably using this command:

```bash
sh -c "$(curl -fsLS https://chezmoi.io/get)"
```

Once chezmoi is installed, you can install these dotfiles by running:

```bash
chezmoi init https://gitlab.com/Danishswag/dotfiles
```

Copyright (c) 2022 Erik Jensen. Released under the MIT License. See [LICENSE.md][license] for details.

[chezmoi]: https://www.chezmoi.io/
[license]: LICENSE.md

