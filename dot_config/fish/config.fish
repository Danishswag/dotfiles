### EXPORT ###
set fish_greeting
set EDITOR "nvim"

### Random Color Script
# From gitlab.com/dwt1/shell-color-scripts
colorscript random

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
eval /home/erik/anaconda/bin/conda "shell.fish" "hook" $argv | source
# <<< conda initialize <<<

### SETTING THE STARSHIP PROMPT ###
starship init fish | source

# bare repo alias for dotfiles
alias config="/usr/bin/git --git-dir=$HOME/ --work-tree=$HOME"
